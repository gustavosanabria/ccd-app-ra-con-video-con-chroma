﻿using UnityEngine;
using System.Collections;

namespace Vuforia
{
    public class FocusMode : MonoBehaviour
    {
#if( _VUFORIA_ANDROID_SETTINGS )
        // Use this for initialization
        void Start()
        {
            VuforiaAbstractBehaviour qcar = (VuforiaAbstractBehaviour)FindObjectOfType(typeof(VuforiaAbstractBehaviour));
            if (qcar)
            {
                qcar.RegisterVuforiaStartedCallback(OnQCARStarted);
            }
            else
            {
                Debug.Log("Failed to find QCARBehaviour in current scene");
            }
        }

        void SetupAutofocus()
        {

            bool focusModeSet = Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);

            if (!focusModeSet)
            {
                focusModeSet = Vuforia.CameraDevice.Instance.SetFocusMode(Vuforia.CameraDevice.FocusMode.FOCUS_MODE_NORMAL);

                if (!focusModeSet)
                    Debug.Log("Failed to set focus mode (unsupported mode).");
                else
                    Debug.Log("Set focus mode as Trigger Auto.");
            }
            else
                Debug.Log("Set focus mode as Continuos Auto.");
        }

        private void OnQCARStarted()
        {
            Debug.Log("Vuforia has started.");

            SetupAutofocus();
        }
#endif
    }
}