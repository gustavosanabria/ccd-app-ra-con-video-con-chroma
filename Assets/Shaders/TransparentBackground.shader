Shader "AlphaVideo/TransparentBackground" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Alpha ("Alpha", float) = 0.333
	_XOffset ("xOffset", Range(0,1)) = 0.5
}

Category {
	Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" }
	Lighting Off ZWrite Off

	BindChannels {
		Bind "Color", color
		Bind "Vertex", vertex
		Bind "TexCoord", texcoord
	}
	
	SubShader {
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha, OneMinusSrcAlpha Zero
			Fog { Color (0,0,0,0) }		
			Cull Off
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Alpha;
			float _XOffset;
						
			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD1;
				float2 texcoord1 : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;
			};
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;
				v.texcoord1 = v.texcoord;

				v.texcoord.y = v.texcoord.y * _XOffset;
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

				v.texcoord1.y = v.texcoord.y + _XOffset;
				o.texcoord1 = TRANSFORM_TEX(v.texcoord1, _MainTex);

				/*
				// para horizontal
				v.texcoord.x = v.texcoord.x * _XOffset;
				o.texcoord = TRANSFORM_TEX(v.texcoord, _MainTex);

				v.texcoord1.x = v.texcoord.x + _XOffset;
				o.texcoord1 = TRANSFORM_TEX(v.texcoord1, _MainTex);
				*/

				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				/* 
				//para horizontal
				float4 prev = tex2D(_MainTex, i.texcoord) * i.color;
				float4 col = tex2D(_MainTex, i.texcoord1);
				*/
				float4 prev = tex2D(_MainTex, i.texcoord1) * i.color;
				float4 col = tex2D(_MainTex, i.texcoord);
				float alpha = (col.r + col.g + col.b) * _Alpha * i.color.a;
				return float4(prev.r, prev.g, prev.b, alpha);
			}
			ENDCG 
		}
	} 	

}
}
