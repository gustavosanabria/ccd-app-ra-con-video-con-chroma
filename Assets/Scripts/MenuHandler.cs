﻿using UnityEngine;
using System.Collections;

public class MenuHandler : MonoBehaviour
{
    public void ChangeMode(int scene)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(scene);
    }
}
